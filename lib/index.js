// Load modules

var Hoek = require('hoek');
var Joi = require('joi');

var Controllers = require('./controllers');


// Declare internals

var internals = {};


exports.register = function (server, options, next) {

    server.route([
        { method: 'GET', path: '/kevin/routeA', config: Controllers.kevin.routeA},
        { method: 'GET', path: '/kevin/routeB', config: Controllers.kevin.routeB},
        { method: 'GET', path: '/kevin/routeC', config: Controllers.kevin.routeC.get},
        { method: 'POST', path: '/kevin/routeC', config: Controllers.kevin.routeC.post},
    ]);

    return next();
};


exports.register.attributes = {
    pkg: require('../package.json')
};
