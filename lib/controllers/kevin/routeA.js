// Load modules


// Declare internals

var internals = {};


module.exports = internals.routeA = {
    description: 'Rotue A of the stevens',
    tags: ['api'],
    handler: function (request, reply) {

        return reply({ status: 'route a ok' });
    }
};
