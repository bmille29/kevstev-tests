// Load modules

var Hoek = require('hoek');
var Joi = require('joi');


// Declare internals

var internals = {
    db: {
        msg: 'The original db message.'
    }
};


module.exports = internals.routeC = {

    get: {
        description: 'GET route C of the stevens',
        tags: ['api'],
        handler: function (request, reply) {

            return reply(internals.db);
        }
    },
    post: {
        description: 'POST route C of the stevens',
        tags: ['api'],
        validate: {
            payload: { data: Joi.object().required() }
        },
        handler: function (request, reply) {

            // Store any changes to the database
            // Obviously it is stupid to use node's memory database but this is just an example
            internals.db = Hoek.merge(internals.db, request.payload.data, true);

            return reply({ status: 'ok' });
        }
    }
};
