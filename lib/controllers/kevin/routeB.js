// Load modules


// Declare internals

var internals = {};


module.exports = internals.routeB = {
    description: 'Route B of the stevens',
    tags: ['api'],
    handler: function (request, reply) {

        return reply({ status: 'route b ok' });
    }
};
