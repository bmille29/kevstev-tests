// Load modules

var Insync = require('insync');
var Code = require('code');
var Lab = require('lab');

var Helper = require('./helper');


// Test shortcuts

var lab = exports.lab = Lab.script();
var describe = lab.describe;
var it = lab.it;
var expect = Code.expect;


// Declare internals

var internals = {};


describe('Kevin individuals', function () {

    it('GETs /kevin/routeA', function (done) {

        var options = {};

        Insync.waterfall([
            Helper.prepareServer(),
            Helper.kevin.routeA(options)
        ], function (err, server, data) {

            expect(err).to.not.exist();

            // Validate output
            var statusCode = data.kevin.routeA.res.statusCode;
            var payload = data.kevin.routeA.res.result;

            expect(statusCode).to.equal(200);
            expect(payload).to.deep.equal({ status: 'route a ok' });

            done();
        });
    });

    it('GETs /kevin/routeB', function (done) {

        var options = {};

        Insync.waterfall([
            Helper.prepareServer(),
            Helper.kevin.routeB(options)
        ], function (err, server, data) {

            expect(err).to.not.exist();

            // Validate output
            var statusCode = data.kevin.routeB.res.statusCode;
            var payload = data.kevin.routeB.res.result;

            expect(statusCode).to.equal(200);
            expect(payload).to.deep.equal({ status: 'route b ok' });

            done();
        });
    });

    it('GETs /kevin/routeC', function (done) {

        Insync.waterfall([
            Helper.prepareServer(),
            Helper.kevin.routeC.get()
        ], function (err, server, data) {

            expect(err).to.not.exist();

            // Validate output
            var statusCode = data.kevin.routeC.get.res.statusCode;
            var payload = data.kevin.routeC.get.res.result;

            expect(statusCode).to.equal(200);
            expect(payload).to.deep.equal({ msg: 'The original db message.' });

            done();
        });
    });

    it('POSTs /kevin/routeC', function (done) {

        var options = {
            payload: {
                data: { msg: 'Kevin is a badass!' }
            }
        };

        Insync.waterfall([
            Helper.prepareServer(),
            Helper.kevin.routeC.post()
        ], function (err, server, data) {

            expect(err).to.not.exist();

            // Validate output
            var statusCode = data.kevin.routeC.post.res.statusCode;
            var payload = data.kevin.routeC.post.res.result;

            expect(statusCode).to.equal(200);
            expect(payload).to.deep.equal({ status: 'ok' });

            done();
        });
    });
});


describe('Kevin\'s flows', function () {

    // README: Route C allows you to set values and read them back.
    it('Tests /kevin/routeC user flow', function (done) {

        var options = {
            payload: {
                data: {
                    msg: 'Kevin is a badass!'
                }
            }
        };

        Insync.waterfall([
            Helper.prepareServer(),
            Helper.kevin.routeC.get(),
            Helper.kevin.routeC.post(options),
            Helper.kevin.routeC.get()
        ], function (err, server, data) {

            expect(err).to.not.exist();

            // Validate output
            var statusCode = data.kevin.routeC.get.res.statusCode;
            var payload = data.kevin.routeC.get.res.result;

            expect(statusCode).to.equal(200);
            expect(payload).to.deep.equal({ msg: 'Kevin is a badass!' });

            done();
        });
    });
});
