// Load modules

var Insync = require('insync');
var Hapi = require('hapi');
var Hoek = require('hoek');
var Joi = require('joi');

var Kevin = require('../../');


// Declare internals

var internals = {
    defaults: { mock: process.env.MOCK === 'true' }
};


module.exports = internals.prepareServer = function (options) {

    return function (next) {

        Insync.auto({
            initialize:      function (next) { return next(null, { options: options || {} }); },
            validateInput:   ['initialize', internals.validateInput],
            //startMockServer: ['validateInput', internals.startMockServer],
            startServer:     ['validateInput', internals.startServer]
        }, function (err, data) {

            var result = {
                initializeMashup: {
                    server: data.startMockServer,
                    services: data.startServer,
                    settings: data.validateInput
                }
            };

            return next(err, data.startServer, result);
        });
    };
};


internals.validateInput = function (next, data) {

    var schema = Joi.object().keys({
        mock: Joi.boolean().default(false).description('Enable mock server (overwrites `MOCK` environmental variable)'),
        // mixer: {
        //     enterprises: Joi.array().unique().items(Joi.string().required()),
        //     mashup: Joi.object().default({}).keys({ origin: Joi.string().uri() })
        // }
    });

    var input = Hoek.applyToDefaults(internals.defaults, data.initialize.options);
    Joi.validate(input, schema, function (err, result) {

        return next(err, result);
    });
};


internals.startMockServer = function (next, data) {

    // TODO: FIX ME (Alwasy skip for now)
    if (!data.validateInput.mock) {
        return next();
    }

    var server = new Hapi.Server();
    server.connection();

    server.register({ register: Mock, options: { plugins: data.validateInput.mixer.enterprises } }, function (err) {

        if (err) {
            return next(err);
        }

        server.start(function (err) {

            return next(err, server);
        });
    });
};


internals.startServer = function (next, data) {

    var input = data.validateInput;
    var mock = data.startMockServer;

    var server = new Hapi.Server();
    server.connection();

    var defaults = {
        //mashup: { origin: Hoek.reach(mock, 'info.uri') }
    };

    // TODO: I know this will error due to plugin validation
    var plugins = [
        { register: Kevin, options: {} }  //Hoek.applyToDefaults(defaults, input.mixer) }
    ];

    server.register(plugins, function (err) {

        if (err) {
            return next(err);
        }

        server.start(function (err) {

            return next(err, server);
        });
    });
};
