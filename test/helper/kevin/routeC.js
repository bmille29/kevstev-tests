// Load modules

var Hoek = require('hoek');


// Declare internals

var internals = {
    defaults: {
        get: {
            method: 'GET',
            url: '/kevin/routeC'
        },
        post: {
            method: 'POST',
            url: '/kevin/routeC',
            payload: {
                data: { msg: 'This is the boring default message.' }
            }
        }
    }
};


module.exports = internals.routeC = {

    get: function (options) {

        return function (server, data, next) {

            options = Hoek.applyToDefaults(internals.defaults.get, options || {}, true);
            server.inject(options, function (res) {

                var result = Hoek.merge(data, { kevin: { routeC: { get: {} } } });
                result.kevin.routeC.get = { req: options, res: res };
                return next(null, server, result);
            });
        };
    },
    post: function (options) {

        return function (server, data, next) {

            options = Hoek.applyToDefaults(internals.defaults.post, options || {}, true);
            server.inject(options, function (res) {

                var result = Hoek.merge(data, { kevin: { routeC: { post: {} } } });
                result.kevin.routeC.post = { req: options, res: res };
                return next(null, server, result);
            });
        };
    }
};
