// Load modules

var Hoek = require('hoek');


// Declare internals

var internals = {
    defaults: {
        method: 'GET',
        url: '/kevin/routeB'
    }
};


module.exports = internals.routeB = function (options) {

    return function (server, data, next) {

        options = Hoek.applyToDefaults(internals.defaults, options || {}, true);
        server.inject(options, function (res) {

            return next(null, server, Hoek.merge(data, { kevin: { routeB: { req: options, res: res } } }));
        });
    };
};
