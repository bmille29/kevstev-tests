## Architecting test cases for `Kevstev`

This module demonstrates both unit test cases and flows.
It makes use of the Async/Insync module and provides a easy mechanism to `copy/paste` `modify input` and set `default` values for any user flow.

_Coming soon:_ Integrating `Smocks` for `offline testing` and `100% code coverage`.
- Mockings guarantees user flow for negative test cases.


## Example test cases

```
describe('Kevin\'s flows', function () {

    // Description: Route C allows you to set values and read them back.
    it('Tests /kevin/routeC user flow', function (done) {

        var options = {
            payload: {
                data: {
                    msg: 'Kevin is a badass!'
                }
            }
        };

        Insync.waterfall([
            Helper.prepareServer(),
            Helper.kevin.routeC.get(),
            Helper.kevin.routeC.post(options),
            Helper.kevin.routeC.get()
        ], function (err, server, data) {

            expect(err).to.not.exist();

            // Validate output
            var statusCode = data.kevin.routeC.get.res.statusCode;
            var payload = data.kevin.routeC.get.res.result;

            expect(statusCode).to.equal(200);
            expect(payload).to.deep.equal({ msg: 'Kevin is a badass!' });

            done();
        });
    });
});
```
